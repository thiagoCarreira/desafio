# desafio

4) Um deadlock é o caso de quando um sistema, ou uma série de sistemas, entram em um loop ao competir por um recurso em comum. Por exemplo, digamos que há 3 etapas(sejam elas no mesmo sistema ou sistemas diferentes); onde o sistema A, para completar sua tarefa, precisa de acesso à um recurso que está sendo utilizado por B, que por sua vez, para completar a tarefa precisa de um recurso que está sendo utilizado por C, que precisa de um recurso que está sendo utilizado por A.
Isso forma um loop A - B - C - A - B -C ..., onde nenhum dos processos finaliza, e consequentemente impede os demais de finalizar.

5)Tanto Streams quanto ParallelStreams são implementações feitas para facilitar a utilização de coleções, listas. Com o objetivo de reduzir o número de linhas necessárias para se obter uma funçao que desempenhe o mesmo papel e com isso tempo de codificação.

A diferença entre os dois se dá na ordem e forma como são processados.
A Stream é iterada sequencialmente, de uma ponta a outra da stream e por uma única thread.
A ParallelStream é iterada parelelamente entre diversas threads.

A parallelstream facilita na questão que não temos que gerenciar threads para realizar processamento em paralelo e é eficiente para grandes quantidades de dados porém é necessário estar atento se a ordem não é importante para determinado caso.

Como a parallelstream vai processar em várias threads, a ordem na qual os itens são processados é comprometida e com isso nem sempre o primeiro será de fato o primeiro a finalizar a execução, o mesmo vale para todos os outros itens presentes na stream.