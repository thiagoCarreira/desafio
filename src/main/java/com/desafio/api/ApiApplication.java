package com.desafio.api;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiApplication {
	
	  @PostConstruct
	  void started() {
	    TimeZone.setDefault(TimeZone.getTimeZone("AMERICA/SAO_PAULO"));
	  }

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
