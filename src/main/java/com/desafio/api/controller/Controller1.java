package com.desafio.api.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.api.dto.CampanhaDTO;
import com.desafio.api.dto.IdDTO;
import com.desafio.api.entity.Campanha;
import com.desafio.api.repository.CampanhaRepository;
import com.desafio.api.service.VigenciaService;

@RestController
public class Controller1 {// TODO renomear a controller

	@Autowired
	private CampanhaRepository campanhaRepository;

	@Autowired
	private VigenciaService vigenciaService;

	@RequestMapping(value = "/incluir", method = RequestMethod.POST)
	public Iterable<Campanha> incluir(@RequestBody CampanhaDTO input) {
		Campanha camp = new Campanha(input);
		List<Campanha> campanhasAjustadas = vigenciaService.ajustaVigencias(camp);
		campanhaRepository.saveAll(campanhasAjustadas);
		return campanhaRepository.findCampanhasVigentesByDataInicio(camp.getDataInicio());
	}

	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public Iterable<Campanha> consultar() {
		return campanhaRepository.findAll();
	}

	@RequestMapping(value = "/atualizar", method = RequestMethod.POST)
	public Campanha atualizar(@RequestBody Campanha input) {// TODO alterar DTO para um com campos variaveis
		Optional<Campanha> campanha = campanhaRepository.findById(input.getId());
		if (campanha.isPresent()) {
			// TODO atualizar os dados da entity
			campanhaRepository.save(campanha.get());
		}
		// TODO adicionar retorno para quando id for invalido ou campanha nao encontrada
		return campanha.get();
	}

	@RequestMapping(value = "/deletar", method = RequestMethod.DELETE)
	public boolean deletar(@RequestBody IdDTO id) {
		campanhaRepository.deleteById(id.getId());
		return true;
	}

}