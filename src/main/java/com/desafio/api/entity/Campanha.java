package com.desafio.api.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.desafio.api.dto.CampanhaDTO;

@Entity
@Table(name = "campanhas")
public class Campanha {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private String nome;

	@NotNull
	private int idTime;

	@NotNull
	private LocalDate dataInicio;

	@NotNull
	private LocalDate dataFim;

	public Campanha() {

	}

	public Campanha(CampanhaDTO input) {
		this.setNome(input.getNome());
		this.setIdTime(input.getIdTime());
		this.setDataInicio(input.getDataInicio());
		this.setDataFim(input.getDataFim());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdTime() {
		return idTime;
	}

	public void setIdTime(int idTime) {
		this.idTime = idTime;
	}

	public LocalDate getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

}
