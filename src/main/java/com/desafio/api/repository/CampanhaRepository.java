package com.desafio.api.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desafio.api.entity.Campanha;

@Repository
public interface CampanhaRepository extends CrudRepository<Campanha, Long> {

	@Query(value = "select * from campanhas where CURRENT_DATE between data_inicio and data_fim;", nativeQuery = true)
	List<Campanha> findCampanhasVigentes();

	@Query(value = "select * from campanhas where ?1 >= data_inicio;", nativeQuery = true)
	List<Campanha> findCampanhasVigentesByDataInicio(LocalDate dataInicio);

	Optional<Campanha> findById(Long id);

}