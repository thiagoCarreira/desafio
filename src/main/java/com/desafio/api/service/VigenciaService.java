package com.desafio.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafio.api.entity.Campanha;
import com.desafio.api.repository.CampanhaRepository;

@Service
public class VigenciaService {

	@Autowired
	private CampanhaRepository campanhaRepository;

	public List<Campanha> ajustaVigencias(Campanha input) {

		List<Campanha> campanhas = campanhaRepository.findCampanhasVigentesByDataInicio(input.getDataInicio());
		for (Campanha campanha : campanhas) {
			campanha.setDataFim(campanha.getDataFim().plusDays(1));
		}

		campanhas.add(input);
		if (campanhas.size() > 0) {
			boolean troca = true;
			while (troca) {
				troca = false;
				for (int i = 0; i < campanhas.size() - 1; i++) {
					if (campanhas.get(i).getDataFim().isEqual(campanhas.get(i + 1).getDataFim())) {
						campanhas.get(i).setDataFim(campanhas.get(i).getDataFim().plusDays(1));
						troca = true;
					}
				}
			}
		}
		return campanhas;
	}
}